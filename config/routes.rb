Rails.application.routes.draw do
  get 'rooms/show'
  root 'dashboard#index'
  get 'home/index'
  get 'room/show'

  resources :friendships, only: [:create, :destroy, :update]
  resources :posts do 
    resources :likes, only: [:create, :destroy]
  	resources :comments do
      resources :likes, only: [:create, :destroy]
      member do 
        get 'view'
      end
    end
  end

  resources :profiles do 
    collection do 
      get 'current_user_friends'
      post 'search_user'
    end
  end

  resources :conversations, only: [] do
    resources :messages , only: [] do 
      collection do 
        get :create 
      end      
    end 
    member do get :close end

    collection do get :open end
  end
  
  devise_for :users, controllers: {registrations: 'registrations'}

  mount ActionCable.server => '/cable'
end
