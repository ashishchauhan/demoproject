# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')
Rails.application.config.assets.paths << Rails.root.join('assets', 'fonts')  


Rails.application.config.assets.precompile += %w( layout/materialdesignicons.css )
Rails.application.config.assets.precompile += %w( layout/vendor.bundle.base.css )
Rails.application.config.assets.precompile += %w( layout/vendor.bundle.addons.css )
Rails.application.config.assets.precompile += %w( layout/style.css )

Rails.application.config.assets.precompile += %w( vendor.bundle.base.js )
Rails.application.config.assets.precompile += %w( vendor.bundle.addons.js )
Rails.application.config.assets.precompile += %w( off-canvas.js )
Rails.application.config.assets.precompile += %w( misc.js )
Rails.application.config.assets.precompile += %w( dashboard.js )

Rails.application.config.assets.precompile += %w( materialdesignicons-webfont.ttf )
Rails.application.config.assets.precompile += %w( materialdesignicons-webfont.woff )



# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )
