class CreateFriendships < ActiveRecord::Migration[5.2]
  def change
    create_table :friendships do |t|
    	t.integer :friend_id
      t.boolean :status
      t.belongs_to :user, index: true
      
      t.timestamps
    end
  end
end
