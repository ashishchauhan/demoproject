class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|

      t.string :content
      t.string :commentor_name
      t.integer :user_id
      t.integer :count
      t.belongs_to :post, index: true

      t.timestamps
    end
  end
end
