class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.belongs_to :user, index: true
      t.integer :recipient_id
      t.string :action

      t.timestamps
    end
  end
end
