App.notifications = App.cable.subscriptions.create "NotificationsChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    $('#notifications-id').prepend(data['notification']);

  notify:  ->
    @perform 'notify'

$(document).on('submit', '.create_post', `function(e) {
  App.notifications.notify();
}`);