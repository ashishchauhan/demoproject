class HomeController < ApplicationController
  def index
    self.per_form_csrf_tokens = true
    session[:conversations] ||= []

    @users = User.all.where.not(id: current_user)
    @conversations = Conversation.includes(:recipient, :messages)
                                 .find(session[:conversations])  
  end
end
