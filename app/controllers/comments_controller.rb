class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :get_post, only: [:edit, :create, :update, :destroy]
  before_action :get_comment, only: [:show, :edit, :update, :destroy]

  def new
    @comment = Comment.new
  end

  def create
    binding.pry
    @comment = @post.comments.new(comment_params)

    respond_to do |format|
      if @comment.save
        format.html { redirect_to root_path, notice: 'Comment was successfully created.' }
        format.js
      else
        format.html { render :new }
        format.js
      end
    end
  end

  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to post_path(@post), notice: 'Comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to post_path(@post), notice: 'Comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def get_comment
    @comment = get_post.find_by(id: params[:id])
  end

  def comment_params
    params.require(:comment).permit(:content).merge({user_id: current_user.id, commentor_name: current_user.first_name})
  end

  def get_post
    @post = current_user.posts.find_by(id: params[:post_id])
  end

end
