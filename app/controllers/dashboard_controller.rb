class DashboardController < ApplicationController
  before_action :authenticate_user!

  def index
    @post, @comment, @comments = Post.new, Comment.new, Comment.all
    @post.pictures.new
    @notifications = current_user.notifications.all
    @user_likes = Like.where(user_id: current_user.id, likeable_type: "Post")
  end
end
