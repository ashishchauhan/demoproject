class FriendshipsController < ApplicationController
  before_action :authenticate_user!
  
  def create
    @friendship = current_user.friendships.new(friendship_params)
    respond_to do |format|
      if @friendship.save
        format.html { redirect_to profiles_path }
        format.js
      else
        format.html { render :new }
        format.js
      end
    end
  end

  def destroy
    current_user.friendships.find_by(id: params[:id]).destroy
    respond_to do |format|
      format.html { redirect_to profiles_path, notice: 'Like destroyed.' }
      format.js
    end
  end

  def update
    @friendship = current_user.inverse_friendships.find_by(id: params[:id])
    respond_to do |format|
      if @friendship.update(friendship_params)
        format.html { redirect_to profiles_path, notice: 'Post was successfully updated.' }
        format.js
      end
    end
  end

  def friendship_params
    params.permit(:friend_id, :status)
  end
end
