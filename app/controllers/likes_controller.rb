class LikesController < ApplicationController
  before_action :get_post, only: [:create, :destroy]

  def create
    @like = @post.likes.new(like_params)

    respond_to do |format|
      if @like.save
        format.html { redirect_to root_path, notice: 'like was successfully created.' }
        format.js
      else
        format.html { render :new }
        format.js
      end
    end
  end

  def destroy    
    @post.likes.find_by(id: params[:id]).destroy
    respond_to do |format|
      format.html { redirect_to root_path, notice: 'Like destroyed.' }
      format.js
    end
  end

private

  def get_post 
    @post = Post.find_by(id: params[:post_id])
  end
  def like_params
    params.permit().merge(user_id: current_user.id)
  end
end
