class ProfilesController < ApplicationController
  before_action :authenticate_user!
  
  def index
    @users = User.all
  end

  def search_user
    @searched_users = User.where("first_name LIKE (?)", "%#{params[:search]}%")
  end
end
