class Post < ApplicationRecord
  belongs_to :user
  has_many :comments
  has_many :likes, as: :likeable
  has_many :pictures, as: :imageable

  accepts_nested_attributes_for :pictures
end
