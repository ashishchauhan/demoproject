class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :posts
  has_many :notifications
  has_many :messages
  has_many :conversations, foreign_key: :sender_id
  has_many :pictures, as: :imageable
  has_many :friendships
  has_many :friends, through: :friendships
  has_many :inverse_friendships, class_name: "Friendship", foreign_key: :friend_id
  has_many :inverse_friends, through: :inverse_friendships, source: :user  


  after_create :signup_confirmation

  def signup_confirmation
  UserMailer.with(user: self).signup_confirmation.deliver_later
  end
end
