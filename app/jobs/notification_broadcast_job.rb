class NotificationBroadcastJob < ApplicationJob
  queue_as :default

  def perform(notification)
    recipient = notification.user

    broadcast_to_recipient(recipient, notification)
  end

  private

  def broadcast_to_recipient(user, notification)
    ActionCable.server.broadcast(
      "notifications:#{user.id}",
      notification: render_notification(notification, user)
    )
  end

  def render_notification(notification, user)
    ApplicationController.renderer.new.render(
      partial: 'notifications/notification',
      locals: { notification: notification, current_user: user }
    )
  end
end
