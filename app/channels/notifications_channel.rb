class NotificationsChannel < ApplicationCable::Channel
  def subscribed    
      stream_from "notifications:#{current_user.id}"
  end

  def unsubscribed
    stop_all_streams
  end

  def notify
    current_user.friends.each do |friend|
      friend.notifications.create(recipient_id: current_user.id, action: "post")
    end
  end
end
